
# Hagglethorn Mods

script to "mod" one of the hagglethorn .stl files

I backed [another one of the Hagglethorn Hollow](https://www.kickstarter.com/projects/printablescenery/hagglethorn-hollow-stl-files) projects.
I now want/ed to tweak some of the files.

My background meant that I'm reluctant to *just* edit the model files, so, here are my edits (without the models themselves) in case someone else wants/needs them.

![demo](https://i.imgur.com/zy813qb.mp4)
