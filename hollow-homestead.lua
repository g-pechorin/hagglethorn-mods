
-- Peter LaValle 
-- https://gitlab.com/g-pechorin/hagglethorn-mods
-- written with/for icesl 2.3.5
-- https://icesl.loria.fr/

--
-- Hagglethorn-Homestead-Base-v1.4.stl needed
-- tick "all volumes" and export the stl file
-- untick "cull" to see the shapes you're using
-- 
shape = load('Hagglethorn-Homestead-Base-v1.4.stl')


brush = Void

full = (ui_bool('all volumes', false))

function cull_cube(edit)
  
  return function (shape)
    -- do we show the cube we're culling?
    if (edit) then
      shape.x = ui_numberBox('x move', shape.x)
      shape.y = ui_numberBox('y move', shape.y)
      shape.z = ui_numberBox('z move', shape.z)

      shape.a = ui_numberBox('x size', shape.a)
      shape.b = ui_numberBox('y size', shape.b)
      shape.c = ui_numberBox('z size', shape.c)
    end

    -- 
    if full or edit then
      brush = union(
        brush,
        translate(shape.x, shape.y, shape.z) * 
          cube(shape.a, shape.b, shape.c))
    end
  end

end

---- first big cube
  cull_cube (false) {
    x = 70, y = 59, z = 0,
    a = 68, b = 62, c = 75,
  }

-- door cube
cull_cube (false) {
    x = 55, y = 77, z = 0,
    a = 40, b = 62, c = 75,
  }

-- big window cube
cull_cube (false) {
    x = 69, y = 43, z = 32,
    a = 28, b = 50, c = 75,
  }



if (ui_bool('cull', true)) then
  shape = difference(shape, brush)
else
  shape = union(shape, brush)
end


emit (shape)
